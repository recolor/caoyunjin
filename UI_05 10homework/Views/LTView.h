//
//  LTView.h
//  UI_05 10homework
//
//  Created by dllo on 16/5/10.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LTView : UIView
@property (nonatomic, retain) UILabel *label;

@property(nonatomic, retain) UIImage *image;







+ (instancetype)ltviewWithFrame:(CGRect)frame
                          title:(NSString *)title content:(NSString *)content name:(NSString *)name;




@end
