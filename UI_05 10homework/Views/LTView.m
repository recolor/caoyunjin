//
//  LTView.m
//  UI_05 10homework
//
//  Created by dllo on 16/5/10.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "LTView.h"

@implementation LTView

+ (instancetype)ltviewWithFrame:(CGRect)frame
                          title:(NSString *)title
                        content:(NSString *)content
                           name:(NSString *)name{
    
    LTView *view = [[LTView alloc] initWithFrame:frame];
   
    
    UILabel *laber1 = [[UILabel alloc] init];
    
    laber1.frame = CGRectMake(frame.size.width / 2 + 10, 0, frame.size.width / 3, frame.size.height / 6);
    laber1.text = title;
    laber1.backgroundColor = [UIColor whiteColor];
    
    [view addSubview:laber1];
    [laber1 release];
    
    UILabel *laber2 = [[UILabel alloc] initWithFrame:CGRectMake(frame.size.width / 2 + 10, frame.size.height / 3, frame.size.width / 2 , frame.size.height / 6)];
    
    laber2.text = content;
    laber2.backgroundColor = [UIColor whiteColor];
    
    [view addSubview:laber2];
    [laber2 release];
    
    UIImageView *image1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width  / 2 - 10, frame.size.height)];
    
    
    image1.image = [UIImage imageNamed:name];
    
    
    [view addSubview:image1];
    [image1 release];
    
    
    
    
    
    
    
    return [view autorelease];
    
    
    
}


@end
