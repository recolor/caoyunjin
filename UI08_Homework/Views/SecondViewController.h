//
//  SecondViewController.h
//  UI08_Homework
//
//  Created by dllo on 16/5/12.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SecondViewControllerDelegate <NSObject>

- (void)changeValue:(NSString *)name;

@end


@interface SecondViewController : UIViewController

@property (nonatomic, copy)NSString *secondText;

@property (nonatomic, assign) id<SecondViewControllerDelegate> secondDelegate;



@end
