//
//  ViewController.m
//  UI08_Homework
//
//  Created by dllo on 16/5/12.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "ViewController.h"
#import "SecondViewController.h"


@interface ViewController ()

@property (nonatomic, retain) UILabel *laber;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor purpleColor];
    [self creatOfOne];
    
    
}

- (void)creatOfOne{
    
    self.laber = [[UILabel alloc] initWithFrame:CGRectMake(40, 100, 300, 50)];
    
    self.laber.backgroundColor = [UIColor orangeColor];
    [self.view addSubview:self.laber];
    [_laber release];
    self.laber.text = @"莱昂纳多·迪卡普里奥";
    self.laber.alpha = 0.5;
    
    
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(100, 200, 100, 100)];
    
    button.backgroundColor = [UIColor orangeColor];
    
    [self.view addSubview:button];
    [button release];
    button.alpha = 0.5;
    
    [button setTitle:@"Second" forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(handleSecond) forControlEvents:UIControlEventTouchUpInside];
    
  
}

- (void)handleSecond{
    
    SecondViewController *second = [[SecondViewController alloc] init];
    second.secondText = self.laber.text;
    
    second.secondDelegate = self;
    
    
    
    [self.navigationController pushViewController:second animated:YES];
    [second release];
    
}

- (void)changeValue:(NSString *)name{

    self.laber.text = name;
    



}




@end
