//
//  ViewController.m
//  UI_0509homework
//
//  Created by dllo on 16/5/9.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "ViewController.h"
#import "UIColorSlider.h"
#import "SimpleView.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatSlider];
    [self exchangeImageButton];
    [self creatseg];
    
    
}

- (void)creatSlider{
    UIColorSlider *slider = [[UIColorSlider alloc] initWithFrame:CGRectMake(self.view.frame.size.width / 2 - 50, 80, 100, 100)];
    slider.maximumValue = 1;
    slider.minimumValue = 0;
    [self.view addSubview:slider];
    [slider release];
    slider.minimumTrackTintColor = [UIColor redColor];
    
    UIColorSlider *slider1 = [[UIColorSlider alloc] initWithFrame:CGRectMake(80, 150, 100, 100)];
    slider.minimumValue = 0;
    slider.maximumValue = 1;
    [self.view addSubview:slider1];
    [slider1 release];
    slider1.minimumTrackTintColor = [UIColor yellowColor];
    
    UIColorSlider *slider2 = [[UIColorSlider alloc] initWithFrame:CGRectMake(80, 300, 100 ,100)];
    slider.minimumValue = 0;
    slider.maximumValue = 1;
    
    [self.view addSubview:slider2];
    [slider2 release];
    slider2.maximumTrackTintColor = [UIColor orangeColor];
    
    UIColorSlider *slider3 = [[UIColorSlider alloc] initWithFrame:CGRectMake(80,400, 100, 100)];
    [self.view addSubview:slider3];
    [slider3 release];
    [slider3 addTarget:self action:@selector(handlealpha:) forControlEvents:UIControlEventValueChanged];
    
    
    
    [slider1 addTarget:self action:@selector(handleColor1:) forControlEvents:UIControlEventValueChanged];
    [slider2 addTarget:self action:@selector(handleColor2:) forControlEvents:UIControlEventValueChanged];
    [slider addTarget:self action:@selector(handleColor:) forControlEvents:UIControlEventValueChanged];
    
 
    
}

- (void)handleColor:(UISlider *)slider{
    self.view.backgroundColor = [UIColor colorWithRed:1 green:slider.value blue:slider.value alpha:slider.value];
    
    
}

- (void)handleColor1:(UISlider *)slider{

    self.view.backgroundColor = [UIColor colorWithRed:slider.value green:0 blue:slider.value alpha:slider.value];
    
}

- (void)handleColor2:(UISlider *)slider{

    self.view.backgroundColor = [UIColor colorWithRed:slider.value green:slider.value blue:0 alpha:slider.value];
}

- (void)handlealpha:(UIColorSlider *)slider{
    self.view.alpha = slider.value;
    



}

#pragma make -


- (void)exchangeImageButton{
    UIButton *butto = [[UIButton alloc] initWithFrame:CGRectMake(90, 500, 100, 100)];
    
    butto.backgroundColor = [UIColor cyanColor];
    [self.view addSubview:butto];
    [butto release];
    [butto setTitle:@"登录" forState:UIControlStateNormal];
    [butto addTarget:self action:@selector(handleimage:) forControlEvents:UIControlEventTouchUpInside];
    
    
 
}
- (void)handleimage:(UIButton *)button{
    SimpleView *view1 = [[SimpleView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    UIButton *butto1 = [[UIButton alloc] initWithFrame:CGRectMake(90, 500, 100, 100)];
    view1.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:view1];
    [view1 release];
    butto1.backgroundColor = [UIColor cyanColor];
    [view1 addSubview:butto1];
    [butto1 release];
    
    [butto1 setTitle:@"注册" forState:UIControlStateNormal];
    
    [butto1 addTarget:self action:@selector(handlechange:) forControlEvents:UIControlEventTouchUpInside];
    
    
   
}

- (void)handlechange:(UIButton *)button{
    SimpleView *view2 = [[SimpleView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    UIButton *butto2 = [[UIButton alloc] initWithFrame:CGRectMake(90, 500, 100, 100)];
    view2.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:view2];
    [view2 release];
    butto2.backgroundColor = [UIColor cyanColor];
    [view2 addSubview:butto2];
    [butto2 release];
        [butto2 setTitle:@"返回" forState:UIControlStateNormal];
    [butto2 addTarget:self action:@selector(handleone:) forControlEvents:UIControlEventTouchUpInside];



}

- (void)handleone:(UIButton *)button{

  [self.view bringSubviewToFront:[self.view viewWithTag:500]];


}

- (void)creatseg{
    UISegmentedControl *seg = [[UISegmentedControl alloc] initWithItems:@[@"注册", @"登录", @"忘记密码"]];
    seg.frame = CGRectMake(300, 400, 100, 100);
    [self.view addSubview:seg];
    [seg release];
    
    [seg addTarget:self action:@selector(handleseg:) forControlEvents:UIControlEventValueChanged];

}

- (void)handleseg:(UISegmentedControl *)seg{
    if (seg.selectedSegmentIndex == 0) {
        
        UIImageView *image1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"副本"]];
        [self.view addSubview:image1];
       
        image1.frame = [UIScreen mainScreen].bounds;
        [image1 release];
           
    }else if (seg.selectedSegmentIndex == 1){
        self.view.backgroundColor = [UIColor cyanColor];
    
    }else if (seg.selectedSegmentIndex == 2){
        self.view.backgroundColor = [UIColor blueColor];
        
    
    }
    




}













@end
