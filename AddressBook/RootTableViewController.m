//
//  RootTableViewController.m
//  AddressBook
//
//  Created by dllo on 16/5/26.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "RootTableViewController.h"
#import "SandBoxStore.h"
@interface RootTableViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, retain) NSMutableDictionary *dic;
@property (nonatomic, retain) NSArray *arrOfKey;
@property (retain, nonatomic) IBOutlet UITableView *tableViewOfRoot;





@end

@implementation RootTableViewController
- (void)dealloc{
    [_dic release];
    [_arrOfKey release];
   
    [_tableViewOfRoot release];
    [super dealloc];
    

}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self handleData];
    
    
}


- (void)handleData{
    NSDictionary *dic = [[SandBoxStore sharedSandBoxStore] readByUnArchiverWithkey:@"contact"];
    NSString *pathOfFile = [[NSBundle mainBundle] pathForResource:@"Ca" ofType:@"plist"];
    self.dic = [NSMutableDictionary dictionaryWithContentsOfFile:pathOfFile];
    
    self.arrOfKey = [_dic.allKeys sortedArrayUsingSelector:@selector(compare:)];
    
    self.tableViewOfRoot.dataSource = self;
    self.tableViewOfRoot.delegate = self;
}





#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return _arrOfKey.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *key = [_arrOfKey objectAtIndex:section];
    NSArray *arr = [_dic objectForKey:key];

    return arr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pool" forIndexPath:indexPath];
    NSString *key = [_arrOfKey objectAtIndex:indexPath.section];
    NSArray *arr = [_dic objectForKey:key];
    
    NSDictionary *dic = [arr objectAtIndex:indexPath.row];
    cell.textLabel.text = dic[@"name"];
    
    
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 80;
    

}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [_arrOfKey objectAtIndex:section];
    

}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
