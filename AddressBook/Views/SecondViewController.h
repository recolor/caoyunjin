//
//  SecondViewController.h
//  AddressBook
//
//  Created by dllo on 16/5/26.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController
@property (retain, nonatomic) IBOutlet UITextField *nameTextFile;
@property (retain, nonatomic) IBOutlet UITextField *ageTextFile;
@property (retain, nonatomic) IBOutlet UITextField *phonenumberTextFile;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *cancelButton;

@end
