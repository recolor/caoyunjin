//
//  SandBoxStore.m
//  UI17_本地存储(SandBox)
//
//  Created by Scott on 16/5/25.
//  Copyright © 2016年 Scott. All rights reserved.
//

#import "SandBoxStore.h"

@implementation SandBoxStore

#pragma mark - Public 
+ (instancetype)sharedSandBoxStore {
    
    static SandBoxStore *sbs = nil;
    if (sbs == nil) {
        sbs = [[SandBoxStore alloc] init];
    }
    
    return sbs;
}
- (NSString *)pathForSandBox {
    
    // Core API:
   return NSHomeDirectory();
    
}
- (NSString *)pathForDocumentDir {
    
    // Core API:
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];

}
- (NSString *)pathForLibrary {
    
    return [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, NO) lastObject];
    
}
- (NSString *)pathForCache {
    
    return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    
}
- (NSString *)pathForTmp {
    
    // Core API
    return NSTemporaryDirectory();
}
- (NSString *)pathForApp {
    
    return [[NSBundle mainBundle] bundlePath];
    
}

#pragma mark - Store
- (BOOL)storeFileWithString:(NSString *)content forKey:(NSString *)key {
    
    NSString *pathOfFile = [[self pathForDocumentDir] stringByAppendingPathComponent:key];
    
    // Core API:
    return [content writeToFile:pathOfFile atomically:YES encoding:NSUTF8StringEncoding error:nil];
}

- (NSString *)readStringForKey:(NSString *)key {
    
    NSString *pathOfFile = [[self pathForDocumentDir] stringByAppendingPathComponent:key];
    
    return [NSString stringWithContentsOfFile:pathOfFile encoding:NSUTF8StringEncoding error:nil];
}

- (BOOL)storeFileWithArray:(NSArray *)content forKey:(NSString *)key {
    
    NSString *path = [[self pathForDocumentDir] stringByAppendingPathComponent:key];
    
   return [content writeToFile:path atomically:YES];
}

- (NSArray *)readArrayForKey:(NSString *)key {
    
    NSString *path = [[self pathForDocumentDir] stringByAppendingPathComponent:key];
    
    return [NSArray arrayWithContentsOfFile:path];
    
}

- (BOOL)storeFileWithDictionary:(NSDictionary *)content forKey:(NSString *)key {
    
    NSString *path = [[self pathForDocumentDir] stringByAppendingPathComponent:key];
    
    return [content writeToFile:path atomically:YES];
}

- (NSDictionary *)readDictionayForKey:(NSString *)key {
    
    NSString *path = [[self pathForDocumentDir] stringByAppendingPathComponent:key];
    
    return [NSDictionary dictionaryWithContentsOfFile:path];

}

- (BOOL)storeWithImage:(UIImage *)image forKey:(NSString *)key {
    
    // Core API
    NSData *data = UIImageJPEGRepresentation(image, 1);
    
    if (!data) {
        data = UIImagePNGRepresentation(image);
    }
    
    return [self storeFileWithData:data forKey:key];
    
}


- (BOOL)storeFileWithData:(NSData *)content forKey:(NSString *)key {
    
    NSString *path = [[self pathForDocumentDir] stringByAppendingPathComponent:key];
    
    return [content writeToFile:path atomically:YES];
}


- (BOOL)storeWithImageData:(NSData *)imageData forKey:(NSString *)key {
    
    return [self storeFileWithData:imageData forKey:key];
    
}

- (BOOL)storeByArchiverWithObject:(id)object forKey:(NSString *)key {
    
    NSMutableData *mData = [NSMutableData data];
    
    // 归档过程
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:mData];
    
    [archiver encodeObject:object forKey:key];
    
    [archiver finishEncoding];
    
    // data存储
    return [self storeFileWithData:mData forKey:key];
}


- (id)readByUnArchiverWithkey:(NSString *)key {
    
    NSString *path = [[self pathForDocumentDir] stringByAppendingPathComponent:key];
    
    // 读取Data
    NSData *data = [NSData dataWithContentsOfFile:path];
    
    // 反归档.
    NSKeyedUnarchiver *unArchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    
    id result = [unArchiver decodeObjectForKey:key];
    
    [unArchiver finishDecoding];
    
    return result;
}



#pragma mark - private



@end






