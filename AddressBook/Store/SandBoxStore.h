//
//  SandBoxStore.h
//  UI17_本地存储(SandBox)
//
//  Created by Scott on 16/5/25.
//  Copyright © 2016年 Scott. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

@interface SandBoxStore : NSObject

/** 单例. */
+ (instancetype)sharedSandBoxStore;
- (NSString *)pathForSandBox;
- (NSString *)pathForDocumentDir;
- (NSString *)pathForLibrary;
- (NSString *)pathForCache;
- (NSString *)pathForTmp;
- (NSString *)pathForApp;

#pragma mark - Store
- (BOOL)storeFileWithString:(NSString *)content forKey:(NSString *)key;
- (NSString *)readStringForKey:(NSString *)key;


- (BOOL)storeFileWithArray:(NSArray *)content forKey:(NSString *)key;

- (NSArray *)readArrayForKey:(NSString *)key;


- (BOOL)storeFileWithDictionary:(NSDictionary *)content forKey:(NSString *)key;

- (NSDictionary *)readDictionayForKey:(NSString *)key;


/** homework */
- (BOOL)storeFileWithData:(NSData *)content forKey:(NSString *)key;

- (BOOL)storeWithImage:(UIImage *)image forKey:(NSString *)key;

- (BOOL)storeWithImageData:(NSData *)imageData forKey:(NSString *)key;

- (BOOL)storeByArchiverWithObject:(id)object forKey:(NSString *)key;

- (id)readByUnArchiverWithkey:(NSString *)key;



@end
