//
//  NewRootViewController.m
//  UI_嵌套
//
//  Created by dllo on 16/6/12.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "NewRootViewController.h"
#import "FirstTableViewController.h"




@interface NewRootViewController () <UICollectionViewDataSource, UICollectionViewDelegate>


@property (nonatomic, retain) UICollectionView *collectionView;

@property (nonatomic, retain) FirstTableViewController *FirstTableView;



@end

@implementation NewRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createCollectionView];
    
    
}

- (void)createCollectionView {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    
    layout.minimumLineSpacing = 0;
    
    
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    layout.itemSize = CGSizeMake(CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds));
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
    
    self.collectionView.pagingEnabled = YES;
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"pool"];
    
    [self.view addSubview:self.collectionView];
    

}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;
    

}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {


    return 3;
    

}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"pool" forIndexPath:indexPath];
    

    cell.backgroundColor =[UIColor blueColor];
    
    self.FirstTableView = [[FirstTableViewController alloc] init];
    
    self.FirstTableView.view.frame = cell.contentView.bounds;
    
    [cell.contentView addSubview:self.FirstTableView.view];
    
    [self addChildViewController:self.FirstTableView];
    
    return cell;
    












}








@end
